# array_change_key_case($array, $case)
Chuyển tất cả các key trong mảng $array sang chữ hoa nếu $case = 1 và sang chữ thường nếu $case = 0. Ta có thể dùng hằng số CASE_UPPER thay cho số 1 và CASE_LOWER thay cho số 0.
# array_combine($array_keys, $array_values)
Trộn 2 mảng $array_keys và $array_values thành một mảng kết hợp với $array_keys là danh sách keys, $array_value là danh sách value tương ứng với key. Điều kiện là 2 mảng này phải bằng nhau.
# array_count_values ( $array )
Đếm số lần xuất hiện của các phần tử giống nhau trong mảng $array và trả về một mảng kết quả.
# array_push(&$array, $add_value1, $add_value2, $add_value…)
Thêm vào cuối mảng $array một hoặc nhiều phần tử với các giá trị tương ứng biến $add_value truyền vào.
# array_pop(&$array)
Xóa trong mảng $array phần tử cuối cùng và trả về phần tử đã xóa.
# array_pad($array, $size, $value)
Kéo dãn mảng $array với kích thước là $size, và nếu kích thước truyền vào lớn hơn kích thước mảng $array thì giá trị $value được thêm vào, ngược lại nếu kích thước truyền vào nhỏ hơn kích thước mảng $array thì sẽ giữ nguyên. Nếu muốn giãn ở cuối mảng thì $size có giá trị dương, nếu muốn giãn ở đầu mảng thì $size có giá trị âm.
# array_shift(&$array)
Xóa phần tử đầu tiên ra khỏi mảng $array và trả về phần tử vừa xóa đó.
# array_unshift(&$array, $value1, $value2, …)
Thêm các giá trị $value1, $value2, … vào đầu mảng $array.\
# is_array($variable).
Kiểm tra một biến có phải kiểu mảng hay không, kết quả trả về true nếu phải và false nếu không phải.
# in_array($needle, $haystackarray)
Kiểm tra giá trị $needle có nằm trong mảng $haystackarray không. trả về true nếu có và flase nếu không có.
# array_key_exists($key, $searcharray)
Kiểm tra key $key có tồn tại trong mảng $searcharray không, trả về true nếu có và false nếu không có.
# array_unique($array)
Loại bỏ giá trị trùng trong mảng $array.
# array_values ($array)
Chuyển mảng $array sang dạng mảng chỉ mục.


