# 1. OOP là gì

OOP là viết tắt của Object-Oriented Programming.

# 2. Classes/Objects

Class là một template của các object, một object là một ví dụ của class.

## 2.1. Định nghĩa một Class

dùng từ khóa `class`\
**VD:**\
`<?php`

`class Fruit {`  \
`  // code goes here...`\
`}`\
`?>`

## 2.2. Định nghĩa một Object

dùng từ khóa `new`\
**VD:**\
`<?php`

`$apple = new Fruit();`\
`$banana = new Fruit();`\
`$apple->set_name('Apple');`\
`$banana->set_name('Banana');`\

`echo $apple->get_name();`\
`echo "<br>";`\
`echo $banana->get_name();`\
`?>`

## 2.3. Từ khóa `$this`

Từ khóa `$this` đề cập đến object hiện tại, và chỉ được dùng bên trong các phương thức\
**VD:**\
`<?php`

`class Fruit {`\
`  public $name;`\
`  function set_name($name) {`\
`    $this->name = $name;`\
`  }`\
`}`\
`$apple = new Fruit();`\
`$apple->set_name("Apple");`\
`?>`

## 2.4. instanceof

từ khóa `instanceof` dùng để kiểm tra xem một object có thuộc về một class xác định nào đó không\
**VD:**\
`$apple instanceof Fruit`

# 3. Constructor

`__construct()` cho phép bạn khởi tạo giá trị của các thuộc tính khi khởi tạo object\
**VD:**

`<?php`

`class Fruit {`

`public $name;`\
`public $color;`

`function __construct($name) {`\
`$this->name = $name;`\
`}`

`}`

# 4. Destructor

`__destruct()` được gọi khi đoạn mã bị dừng hoặc thoát
**VD:**

`<?php`\
`class Fruit {`

`public $name;`\
`public $color;`

`function __construct($name) {`\
`$this->name = $name;`\
`}`\
`function __destruct() {`\
`echo "The fruit is {$this->name}.";`\
`}`\
`}`

`$apple = new Fruit("Apple");`\
`?>`

# 5. Access Modifiers

- `public`: thuộc tính và phương thức truy cập được ở bất cứ đâu.
- `protected`: thuộc tính và phương thức có thể truy cập được trong class và những class kế thừa nó.
- `private`: thuộc tính và phương thức chỉ có thể truy cập trong class.

# 6. Inheritance

## 6.1. Khai báo

khai báo class kế thừa bằng từ khóa `extends`\
**VD:**\
`<?php`\
`class Fruit {`\
`// code inside`\
`}`

// Strawberry is inherited from Fruit\
`class Strawberry extends Fruit {`\
`// code inside`\
`}`\
`?>`

## 6.2. Quyền truy cập `protected`

## 6.3. Ghi đè phương thức kế thừa

Có thể ghi đè phương thức của class cha bằng cách đặt trùng tên

## 6.4. Từ khóa `final`

### 6.4.1 `final class`

**VD:**\
`<?php`\
`final class Fruit {`\
`  // some code`\
`}`

// đoạn code dưới sẽ có lỗi vì class Fruit được khai báo với từ khóa final, không class nào có thể kế thừa class đó nữa\
`class Strawberry extends Fruit {`\
`  // some code`\
`}`\
`?>`

### 6.4.2 final methods

Phương thức được khai báo với từ khóa final thì không thể ghi đè

# 7. Class constants

- các thuộc tính được khai báo với từ khóa `const`
  thì không thể thay đổi giá trị
- nên đặt tên các constant bằng tất cả cữ cái viết hoa, ngăn cách bởi dấu gạch dưới
- có thể truy cập constant bên ngoài class bằng tên class + 2 dấu hai chấm (::), hoặc trong class bằng từ khóa
  `self` + 2 dấu hai chấm (::)

# 8. Abstract classes

- class và phương thức trừu tượng là khi class cha có những phương thức được đặt tên, nhưng phải để class con định nghĩa
  nhiệm vụ.
- class trừu tượng phải có ít nhất 1 phương thức trừu tượng.
- phương thức trừu tượng phải được định nghĩa, nhưng không bao gôm code
- để khai báo, sử dụng từ khóa `abstract`

# 9. Interfaces

## 9.1. What is interfaces?

- cho phép xác định những phương thức gì mà một class triển khai.
- giúp dễ dàng sử dụng nhiều class khác nhau theo cùng 1 cách, khi một hoặc nhiều class dùng cùng 1 interface, đò gọi là
  tính đa hình.
- định nghĩa bằng từ khóa `interface`

## 9.2. Interfaces vs Abstract classes

- interface không thể có thuộc tính, class trửu tượng thì có,
- tất cả phương thức của interface phải là public, còn của class trừu tượng thì có thể là public hoặc protected.
- tất cả các phương thức của interface đều là trừu tượng, nên không cần từ khóa `abstract`
- class có thể vừa triển khai một interface, vừa kế thừa một class khác

## 9.3. Using interface

- để triển khai một interface, class phải sử dụng từ khóa `implements`
- class khi triển khai một interface thì **phải** triển khai toàn bộ phương thức của interface đó.

# 10. Traits

## 10.1. What are traits?

- sinh ra đẻ giúp một class có thể kế thừa nhiều hành vi.
- có thể có các phương thức và các phương thức trừu tượng, có thể có bất kỳ quyền truy cập nào.
- khai báo với từ khóa `trait`\
  **Syntax:**

`<?php`

`trait TraitName {`\
`  // some code...`\
`}`\
`?>`

- để dùng trait trong class, dùng từ khóa `use`\
  **Syntax:**

`<?php`

`class MyClass {`\
`  use TraitName;`\
`}`\
`?>`

# 11. Static methods

## 11.1. What is static method?

- phương thức tĩnh có thể được gọi thẳng mà không cần tạo instance object
- khai báo bằng từ khóa `static`

## 11.2. Syntax

- Gọi ở ngoài class\
  **Syntax:**\
  `ClassName::methodName()`
- Gọi ở trong class\
  **Syntax:**\
  `self::methodName()`
- Gọi ở trong class con thừa kế\
  **Syntax:**\
  `parent::methodName()`  
# 12. Static properties (giống static methods)
# 13. Namespaces
- Giải quyết 2 vấn để:
> + Tổ chức code tốt hơn bằng cách nhóm các class làm chung một nhiệm vụ lại với nhau.
> + Cho phép dùng một tên cho nhiều class.

- Khai báo bằng cách dùng từ khóa `namespace`
- phải khai báo ngay đầu tiên file.
- để sử dung, sử dụng từ khóa `use` 

  


