# Basic Use of Exceptions
`throw new Exception('...')`
# Try, throw and catch
`try { `\
`    // code may cause error`\
`} catch(Exception $e) {`\
`    // handle error`\
`}`

# Creating a Custom Exception Class
`class customException extends Exception {`\
`// ...`\
`}`
# Multiple Exceptions
# Re-throwing Exceptions
# Set a Top Level Exception Handler
The `set_exception_handler()` function sets a user-defined function to handle all uncaught exceptions:\
`set_exception_handler(exception_name)`