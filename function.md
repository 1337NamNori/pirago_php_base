# PHP Built-in Functions
# PHP User defined functions
**Syntax:**\
`function functionName() {`\
`//  code to be executed;`\
`}`
# PHP Function Arguments
- specify `strict`:\
` declare(strict_types=1);`
# PHP Default Argument Value
# PHP Functions - Returning values
# PHP Return Type Declarations
`<?php`

` declare(strict_types=1);`\
`function addNumbers(float $a, float $b) : float {`\
`return $a + $b;`\
`}`\
`echo addNumbers(1.2, 5.2);`\
`?>`
# Passing Arguments by Reference
`<?php`

`function add_five(&$value) {`\
`  $value += 5;`\
`}`\
`$num = 2;`\
`add_five($num);`\
`echo $num;`\
`?>`