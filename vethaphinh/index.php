<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css"
          integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
</head>
<body>
<div class="container mt-5">
    <h3>Vẽ hình tháp</h3>
    <a href="/tam_giac">Vẽ hình tam giác</a>
    <br>
    <a href="/tam_giac_nguoc">Vẽ hình tam giác ngược</a>
    <br>
    <a href="/tam_giac_rong">Vẽ hình tam giác rỗng</a>
    <br>
    <a href="/hinh_vuong">Vẽ hình vuông</a>
    <br>
    <a href="/hinh_vuong_rong">Vẽ hình vuông rỗng</a>
</div>
</body>
</html>
