<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css"
          integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Roboto+Mono&display=swap" rel="stylesheet">
    <style>
        * {
            font-family: 'Roboto Mono', monospace;
        }
    </style>
</head>
<body>
<?php
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $row = $_POST['row'];

    if ($row <= 1 || !is_int(+$row)) {
        $error = 'Vui lòng nhập số nguyên dương lớn hơn 1';
    } else {
        session_start();
        $_SESSION['row'] = $row;
        session_write_close();
        header('Location: /tam_giac/result.php');
        exit();
    }
}
?>
<div class="container mt-5">
    <a href="/">Home</a>
    <h3>Vẽ hình tam giác</h3>
    <form action="" method="POST">
        <div class="form-group">
            <label for="row">Nhập chiều cao của hình tam giác</label>
            <input type="text" class="form-control" id="row" name="row" value="<?php echo $row ?? '' ?>">
            <?php if ($error): ?>
                <small class="text-danger">
                    <?php echo $error; ?>
                </small>
            <?php endif; ?>
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
</div>
</body>
</html>