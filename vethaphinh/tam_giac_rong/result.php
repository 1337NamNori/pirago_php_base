<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Result</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css"
          integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Roboto+Mono&display=swap" rel="stylesheet">
    <style>
        * {
            font-family: 'Roboto Mono', monospace;
        }
    </style>
</head>
<body>
<?php
session_start();
$edge = $_SESSION["edge"];
?>
<div class="container mt-5">

    <a href="/">Home</a>
    <br>
    <a href="index.php" class="mb-3">Back</a>
    <h3 class="mb-5">Hình tam giác rỗng có chiều dài đáy là <?php echo $edge; ?></h3>
    <?php

    for ($i = ($edge - 1) / 2; $i >= 0; $i--) {
    // create a row
        for ($j = 0; $j < $edge; $j++) {
            $isInEdge = $i === 0 || $j === $i || $j === $edge - $i - 1;
            if ($isInEdge) {
                echo "*&nbsp;";
            } else {
                echo "&nbsp;&nbsp;";
            }
        }
        echo '<br>';
    }
    ?>
</div>
</body>
</html>
