# While loop
**Syntax**:\
`while (condition is true) {`\
`// code to be executed`\
`}`
# Do while loop
**Syntax:**\
`do {`\
`//  code to be executed;`\
`} while (condition is true);`
# For loop
**Syntax:**\
`for (init counter; test counter; increment counter) {`\
`//  code to be executed for each iteration;`\
`}`
# Foreach loop
**Syntax:**\
`foreach ($array as $value) {`\
`//  code to be executed;`\
`}`