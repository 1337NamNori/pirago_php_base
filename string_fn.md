# Concat
use . or .=
# Regular string functions
## strlen($string)
- trả về số ký tự của chuỗi
## str_word_count($string)
- trả về số từ trong chuỗi
## str_repeat($string, $repeat)
- lặp chuỗi $string lên $repeat lần
## str_replace($search, $replace, $string)
- Hàm tìm kiếm chuỗi $search trong chuỗi $string và thay thế bằng chuỗi $search trong chuỗi $string bằng chuỗi $replace.
## substr($string, $start, $length)
- Hàm này có tác dụng cắt chuỗi $string, cắt toàn bộ các phần của chuỗi trừ điểm bắt đầu ở vị trí $start đếm $length ký tự sẽ được giữ lại.
## addcslashes($string, $charlist)
- Hàm addcslashes() trả về một chuỗi có dấu \ được chèn phía trước các ký tự của chuỗi $string mà các ký tự này được chỉ định trong chuỗi $charlist.
## addslashes($string)
- Hàm có tác dụng chèn ký tự \ vào trước ký tự " hoặc ký tự ' nếu có trong chuỗi $string.
## strtoupper($string)
- Sử dụng hàm để chuyển các ký tự trong chuỗi thành chữ hoa.
## strtolower($string)
- Sử dụng hàm để chuyển các ký tự trong chuỗi thành chữ thường.
## ucwords($string)
- Sử dụng hàm này để chuyển các ký tự đầu tiên của các từ trong chuỗ $string thành chữ hoa.
## ucfirst($string)
- Sử dụng hàm để chuyển ký tự đầu tiên của chuỗi $string thành chữ hoa.
# Regular Expressions
## preg_match($pattern, $subject, $matches)
## Một số quy tắc khai báo chuỗi $pattern
- Khi khai báo chuỗi $pattern luôn phải đặt chuỗi trong cặp dấu / /.
- Nếu muốn so khớp tất cả của 1 chuỗi $subject với 1 $pattern thì phải sử dụng thêm cặp ^ (bắt đầu) và $ (kết thúc) ở bên trong dấu //.

|pattern|description                                            |
|-------|-------------------------------------------------------|
| [a-z] | chuỗi đầu vào phải là in thường gồm các ký tự từ a->z |
| [A-Z] | chuỗi đầu vào phải là in hoa gồm các ký tự từ A->Z    |
| [0-9] | chuỗi đầu vào phải là các số từ 0->9                  |
