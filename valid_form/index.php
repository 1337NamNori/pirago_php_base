<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css"
          integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <title>Document</title>
</head>
<body>

<?php
$error = [];
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $email = trim($_POST['email']);
    $password = trim($_POST['password']);
    if (strlen($email) === 0) {
        $error['email'] = 'Please enter an email';
    }

    if (strlen($email) > 0 && !filter_var($email, FILTER_VALIDATE_EMAIL)) {
        $error['email'] = 'Please enter a valid email';
    }
    if (strlen($password) === 0) {
        $error['password'] = 'Please enter a password';
    }
    if (empty($error)) {
        session_start();
        $_SESSION = $_POST;
        session_write_close();
        header("Location: data.php");
        exit ();
    }
}
?>

<div class="container mt-5">
    <form action="" method="POST">
        <div class="form-group">
            <label for="email">Email address</label>
            <input type="text" class="form-control" id="email" name="email" value="<?php echo $email ? $email : ''; ?>">
            <?php if ($error['email']): ?>
                <small class="text-danger">
                    <?php echo $error['email']; ?>
                </small>
            <?php endif; ?>
        </div>
        <div class="form-group">
            <label for="password">Password</label>
            <input type="password" class="form-control" id="password" name="password">
            <?php if ($error['password']): ?>
                <small class="text-danger">
                    <?php echo $error['password']; ?>
                </small>
            <?php endif; ?>
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
</div>

</body>
</html>