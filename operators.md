# Arithmetic operators
| Operator| Name          |
|---------|---------------|
| +:	  | Addition	  |
| -:	  | Subtraction	  |
| *:	  | Multiplication|	
| /:	  | Division	  |
| %:	  | Modulus 	  |
| **:	  | Exponentiation|

# Assignment operators
| Assignment| Same as     |
|-----------|-------------|
| x = y	    | x = y		  |
| x += y	| x = x + y	  |	
| x -= y	| x = x - y   |		
| x *= y	| x = x * y	  |	
| x /= y	| x = x / y	  |	
| x %= y	| x = x % y   |

# Comparison operators
| Operator | Name                 |
|----------|----------------------|
| ==       | Equal                | 
| ===      | Identical            |
| !=       | Not Equal            |
| !==      | Not Identical        |
| >        | Greater Than         |
| <        | Less Than            |
| >=       | Greater Than Or Equal|
| <=       | Less Than Or Equal   |
| <=>      | Spaceship            |
> Spaceship:
$x <=> $y \
if $x > $y return 1\
if $x = $y return 0\
if $x < $y return -1 
# Increment/Decrement operators
| Operator | Name            |
|----------|-----------------|
| ++$x     | Pre-increment   | 
| $x++     | Post-increment  |
| --$x     | Pre-decrement   |
| $x--     | Post-decrement  |

# Logical operators
| Operator | Name  |
|----------|-------|
| and      | And   | 
| or       | Or    |
| xor      | Xor   |
| &&       | And   |
| ||       | Or    |
| !        | Not   |

# String operators
| Operator | Name                     |
|----------|--------------------------|
| .        | Concatenation            |
| .=       | Concatenation assighment | 

# Array operators
| Operator | Name                 |
|----------|----------------------|
| +        | Union                |
| ==       | Equal                | 
| ===      | Identical            |
| != or <> | Not Equal            |
| !==      | Not Identical        |

# Conditional assignment operators
| Operator | Name             |
|----------|------------------|
| ?:       | Ternary          |
| ??       | Null coalescing  | 